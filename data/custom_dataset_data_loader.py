import torch.utils.data
from data.base_data_loader import BaseDataLoader

from support_scripts.components import CityScapesDataset


def CreateDataset(opt):
    dataset = None
    from data.aligned_dataset import AlignedDataset

    dataset = AlignedDataset()

    print("dataset [%s] was created" % (dataset.name()))
    dataset.initialize(opt)
    return dataset


class CustomDatasetDataLoader(BaseDataLoader):
    def __init__(self, opt):
        self.height_width: tuple = opt.input_height_width
        self.root: str = opt.dataroot
        self.split: str = "train"
        pass

    def name(self):
        return "CustomDatasetDataLoader"

    def initialize(self, opt):
        BaseDataLoader.initialize(self, opt)

        dataset_features_dict: dict = {
            "instance_map": True,
            "instance_map_processed": False,
            "feature_extractions": {"use": False, "file_path": None},
        }

        self.dataset = CityScapesDataset(
            self.height_width,
            self.root,
            self.split,
            should_flip=not opt.no_flip,
            subset_size=opt.max_dataset_size,
            noise=False,
            dataset_features=dataset_features_dict,
            specific_model="pix2pixHD",
        )
        # self.dataset = CreateDataset(opt)
        self.dataloader = torch.utils.data.DataLoader(
            self.dataset,
            batch_size=opt.batchSize,
            shuffle=not opt.serial_batches,
            num_workers=int(opt.nThreads),
        )

    def load_data(self):
        return self.dataloader

    def __len__(self):
        return min(len(self.dataset), self.opt.max_dataset_size)
