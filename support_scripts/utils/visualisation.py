from typing import Tuple

import numpy as np
import torch
from matplotlib import pyplot as plt


def fast_pca(data: torch.Tensor) -> Tuple[torch.Tensor, torch.Tensor, torch.Tensor]:
    """
    Perform the fast PCA used for eigenfaces.

    :param data_flat: (torch.Tensor): 3 channel hyper-spectral image data.
    :return: (torch.Tensor, torch.Tensor, torch.Tensor): Eigen values(maybe), Eigen vectors, and mean of data.
    """
    data_flat = data.detach().clone()
    data_flat = data_flat.flatten(start_dim=1)
    num_images: int = data_flat.shape[0]
    num_pixels: int = data_flat.shape[1]

    mean = torch.mean(data_flat, 0, keepdim=True)
    centred_data: torch.Tensor = data_flat - mean

    cov_matrix = (1.0 / (num_images - 1)) * torch.matmul(centred_data, centred_data.T)

    eigen_values_l, eigen_vectors_l = torch.symeig(cov_matrix, eigenvectors=True)

    eigen_values_l = eigen_values_l[torch.argsort(eigen_values_l, descending=True)]
    eigen_vectors_l = eigen_vectors_l[torch.argsort(eigen_values_l, descending=True)]

    eigen_vectors = torch.matmul(eigen_vectors_l, centred_data)[:num_pixels][
        :num_images
    ]

    return eigen_values_l, eigen_vectors, mean


def eigenvector_visualisation(self, input: tuple, output: torch.Tensor) -> None:

    if output.shape[2] != 128:
        return

    resolution: int = 2048

    # Compute ordered eigenvalues and eigenvectors
    eigen_values, eigen_vectors, data_mean = fast_pca(output[0])

    eigen_values = eigen_values.cpu()
    eigen_vectors = eigen_vectors.cpu()
    data_mean = data_mean.cpu()

    # plt.plot(eigen_values.cpu())
    # plt.show()

    img_shape = tuple(output.shape[2:])

    eigval_mean = eigen_values.mean().item()
    eigval_max: int = eigen_values.max().item()

    subset = len(eigen_values[eigen_values > eigen_values.mean()])

    tan_h = torch.tanh_

    x_vals = []
    y_vals = []
    colour_vals = []
    z_vals = []

    eigen_pic: torch.Tensor
    for index, eigen_pic in enumerate(eigen_vectors + data_mean):

        if index < subset:
            # eigen_pic = tan_h(eigen_pic)
            eigen_pic = (
                ((eigen_pic - eigen_pic.min()) / eigen_pic.norm()) * resolution
            ).int()
            # subset = eigen_pic - eigen_pic.mode()
            eigen_mode = eigen_pic.mode()[0].int()
            eigen_pic = torch.abs(eigen_pic - eigen_mode)
            eigen_pic = eigen_pic.reshape(img_shape)

            max_val = eigen_pic.max().item()

            rows: int = eigen_pic.shape[0]
            cols: int = eigen_pic.shape[1]

            index_y = [i for i in range(cols) for _ in range(rows)]
            index_x = [i for _ in range(cols) for i in range(rows)]
            # test = eigen_pic[index_x, index_y]
            colour_val: np.ndarray = (
                (eigen_pic[index_x, index_y].float() / max_val).neg() + 1
            ).numpy()

            alpha_val = ((colour_val * -1) + 1) * (
                eigen_values[index].item() / eigval_max
            )
            colour_vals.extend(list(zip(colour_val, colour_val, colour_val, alpha_val)))

            x_vals.extend([rows - x for x in index_x])
            y_vals.extend(index_y)
            z_vals.extend([index] * (rows * cols))

    x_vals_np = np.array(x_vals)[:, np.newaxis]
    y_vals_np = np.array(y_vals)[:, np.newaxis]
    z_vals_np = np.array(z_vals)[:, np.newaxis]
    colour_vals_np = np.array(colour_vals)

    combined_np = np.concatenate(
        (x_vals_np, y_vals_np, z_vals_np, colour_vals_np), axis=1
    )
    choices = np.random.choice(
        combined_np.shape[0], combined_np.shape[0] // (output.shape[2] // 3)
    )

    reduced_combined_np = combined_np[choices]

    fig = plt.figure()
    ax: plt.axes
    if subset > 1:
        ax = fig.add_subplot(111, projection="3d")
        ax.scatter(
            reduced_combined_np[:, 1],
            reduced_combined_np[:, 2],
            reduced_combined_np[:, 0],
            c=reduced_combined_np[:, 3:],
            s=40,
        )
        ax.set_zlim(0, img_shape[0])
    else:
        ax = fig.add_subplot(111)
        ax.scatter(
            reduced_combined_np[:, 1],
            reduced_combined_np[:, 0],
            c=reduced_combined_np[:, 3:],
            s=40,
        )

    ax.set_xlim(0, img_shape[1])
    print(self)
    plt.gray()
    plt.show()

    # import os
    # if not os.path.exists("./saved_plots/"):
    #     os.mkdir("./saved_plots/")
    #
    # for angle in range(0, 360):
    #     ax.view_init(30, angle)
    #     plt.draw()
    #     plt.savefig("./saved_plots/img_{}".format(str(angle)))

    # new_shape = (1, *output.shape[2:])
    # subset = (eigen_vectors[2:3] + mean).reshape(output.shape[2:])
    # #subset = subset.permute(1,2,0)
    # plt.imshow(subset.cpu().numpy(), cmap="Greys")
    # plt.show()
    pass


def analyse_activations(
    self: torch.nn.modules.conv.Conv2d, input: tuple, output: torch.Tensor
) -> None:
    print(self.weight.shape)
    data = output[0].detach().cpu()
    mean = torch.mean(data, 0, keepdim=True)
    diff = data - mean
    summed = torch.sum(diff.abs(), dim=(1, 2))
    order = summed.argsort(descending=True)
    data_sorted = data[order]
    plt.imshow(data_sorted[:3].permute(1, 2, 0).cpu().numpy())
    plt.show()
    print(summed.shape)
    # pca(self, input, output)

    pass
